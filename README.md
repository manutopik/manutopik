# 2JggyyUn2puL5PG6jsMYFC2y9KwjjMmy2adnx3c5fUf8

[Afficher sur cesium](https://demo.cesium.app/#/app/wot/2JggyyUn2puL5PG6jsMYFC2y9KwjjMmy2adnx3c5fUf8/Emmanuel%20Salomon)  • 
[Afficher sur la carte](https://carte.monnaie-libre.fr?pubkey=2JggyyUn2puL5PG6jsMYFC2y9KwjjMmy2adnx3c5fUf8)

**N'hésitez pas à faire un don en Ğ1 😍**

# Contributions

- Développement et rédaction du site [monnaie-libre.fr](https://monnaie-libre.fr) (Dépôt [du site](https://git.duniter.org/websites/monnaie-libre-fr), [de l'api](https://git.duniter.org/websites/api-monnaie-libre))
- Développement de la [carte.monnaie-libre.fr](https://carte.monnaie-libre.fr) ([Dépôt](https://git.duniter.org/websites/carte-g1))
- [Doc silkaj](https://git.duniter.org/websites/doc_silkaj)
- [WotWizard-UI](https://git.duniter.org/clients/wotwizard-ui)
- [g1lib](https://git.duniter.org/libs/g1lib.js)
- [Duniter UI avec nuxt](https://git.duniter.org/nodes/typescript/modules/duniter-ui-nuxt) `Abandonné :/`

# En cours

- [Extension web g1Companion](https://git.duniter.org/clients/g1companion)
- [Indexer pour duniter v2s](https://git.duniter.org/manutopik/duniter-indexer)
- Interface web pour g1Billet
